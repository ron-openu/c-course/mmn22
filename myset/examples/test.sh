#! /bin/bash

PROG=myset

for input in *.input ; do
  echo ~~~~~~~~~~~~~~~ $input ~~~~~~~~~~~~~~~
  output="${input%.input}.output"
  ../$PROG < $input > $output
  cat $output
done
