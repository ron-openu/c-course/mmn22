/******************************************************************************
 * File:    myset.c
 * Author:  Ron Hasson
 * Purpose: the main program - an interactive 'calculator' for operating on sets.
 ******************************************************************************/

/******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "set.h"

/******************************************************************************
 *** TYPEDEFS
 ******************************************************************************/

#define MAX_LINE_LENGTH 4096
#define NUM_OF_SETS 6
#define ARRAY_LENGTH(array) (sizeof((array)) / sizeof((array)[0]))

/* the type of the inputted token identified */
typedef enum MYSET_TOKEN_TYPE
{
   COMMAND = 0,
   TOKEN_SET,
   NUMBER,
   COMMA,
   MINUS1,
   END_OF_LINE,
   INVALID
} MYSET_TOKEN_TYPE;

/* the type of the command token that are supported */
typedef enum MYSET_TOKEN_COMMAND
{
   COMMAND_STOP = 0,
   COMMAND_PRINT,
   COMMAND_READ,
   COMMAND_UNION,
   COMMAND_INTERSECT,
   COMMAND_SUB,
   COMMAND_SYMDIFF
} MYSET_TOKEN_COMMAND;

/*
 * Token Struct - contains the type of the token and is value.
 * The 'value' field is used differently depending on the type of token.
 * For example, if the token type is 'NUMBER', the 'value' field will hold the integer value of the number.
 * If the token type is 'COMMAND', the 'value' field will hold the corresponding command value.
 * If the token type is 'TOKEN_SET', the 'value' field will hold the index of the set (0-5).
 */
typedef struct MYSET_TOKEN
{
   MYSET_TOKEN_TYPE eType;
   int value;
} MYSET_TOKEN;

/******************************************************************************
 *** INTERNAL FUNCTIONS DECLARATIONS
 ******************************************************************************/
static int myset_InitializeSets();
static void myset_FreeSets();
static char *myset_GetLine();
static int myset_ProcessLine();
static MYSET_TOKEN myset_GetNextToken(char *line, int lineLength, int *currentPosition);

/******************************************************************************
 *** GLOBAL VARIABLES
 ******************************************************************************/
static HSET g_myset_Set[NUM_OF_SETS];

/******************************************************************************
 *** EXTERNAL FUNCTIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    main
 * Purpose: see program's purpose above
 * Params:  None
 * Returns: 0 on success, -1 on failure
 ******************************************************************************/
int main(void)
{
   int status = 0;

   if (myset_InitializeSets())
   {
      myset_FreeSets();
      return -1;
   }

   while (status == 0)
   {
      status = myset_ProcessLine();
      printf("\n");
   }

   myset_FreeSets();

   return 0;
}

/******************************************************************************
 *** INTERNAL FUNCTIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    myset_InitializeSets
 * Purpose: Initializes the sets array with 6 empty sets.
 * Params:  None
 * Returns: 0 on success, -1 on failure
 ******************************************************************************/
static int myset_InitializeSets()
{
   int i;
   for (i = 0; i < ARRAY_LENGTH(g_myset_Set); i++)
   {
      g_myset_Set[i] = SET_Create();
      if (NULL == g_myset_Set[i])
      {
         return -1;
      }
   }
   return 0;
}

/******************************************************************************
 * Func:    myset_FreeSets
 * Purpose: Free the sets
 * Params:  None
 * Returns: None
 ******************************************************************************/
static void myset_FreeSets()
{
   int i;
   for (i = 0; i < ARRAY_LENGTH(g_myset_Set); i++)
   {
      if (NULL != g_myset_Set[i])
      {
         SET_Free(g_myset_Set[i]);
      }
   }
}
/******************************************************************************
 * Func:    myset_PrintSet
 * Purpose: Prints the elements of a set to the console.
 * Params:  hset - The handle to the set to be printed.
 * Returns: None
 ******************************************************************************/
static void myset_PrintSet(HSET hset)
{
   int *setArray;
   int setLength;
   int i = 1;
   setArray = SET_Export(hset, &setLength);
   if (setLength == 0)
   {
      printf("The set is empty\n");
      return;
   }
   printf("%d", setArray[0]);
   for (i = 1; i < setLength; i++)
   {
      printf(",%d", setArray[i]);
   }
   printf("\n");
}

/******************************************************************************
 * Func:    myset_GetLine
 * Purpose: Function to get a line from the standard input.
 * Params:  None
 * Returns: A pointer to the line read from the standard input.
 * Remark:  the caller function needs to free the returned pointer
 ******************************************************************************/
static char *myset_GetLine()
{
   char *line = NULL;
   line = malloc(sizeof(char) * MAX_LINE_LENGTH);
   if (NULL == line)
   {
      return NULL;
   }
   if (NULL == fgets(line, MAX_LINE_LENGTH, stdin))
   {
      free(line);
      return NULL;
   }
   return line;
}

/******************************************************************************
 * Func:    myset_ProcessLine
 * Purpose: Processes a single line of input from the user.
 * Params:  None
 * Returns: 0  - on success
 *          -1 - on failure (stops program)
 *          1  - on normal stop command
 ******************************************************************************/
static int myset_ProcessLine()
{
   char *line = NULL;
   int currentPosition = 0;
   int lineLength = 0;
   MYSET_TOKEN token;
   MYSET_TOKEN_COMMAND command;
   HSET hSetFirst;
   HSET hSetSecond;
   HSET hSetThird;
   int nRetVal = 0;
   int *numbersArr = NULL;

   /*get the line input from the user */
   printf(">");
   line = myset_GetLine();

   /* stop the program on unexpected EOF or memory allocation failed */
   if (NULL == line)
   {
      if (feof(stdin))
      {
         printf("End of file reached without 'stop'\n");
      }
      else
      {
         printf("Memory allocation failed\n");
      }
      nRetVal = -1;
      goto lblCleanup;
   }

   /* print the inputted line in order for it to show with file redirection */
   printf(")%s", line);

   lineLength = strlen(line);
   token = myset_GetNextToken(line, lineLength, &currentPosition);

   /* EXPECTING COMMAND OR END_OF_LINE (EMPTY LINE) */
   if (token.eType == END_OF_LINE)
   {
      goto lblCleanup;
   }
   if (token.eType != COMMAND)
   {
      printf("Invalid Token, command is expected\n");
      goto lblCleanup;
   }

   /* command stop */
   if (token.value == COMMAND_STOP)
   {
      token = myset_GetNextToken(line, lineLength, &currentPosition);
      if (token.eType == END_OF_LINE)
      {
         nRetVal = 1;
         goto lblCleanup;
      }
      printf("Invalid Token, end of line is expected\n");
      goto lblCleanup;
   }

   command = token.value;
   /* We are processing non-stop command. so the next token should be a set name */
   token = myset_GetNextToken(line, lineLength, &currentPosition);
   if (token.eType != TOKEN_SET)
   {
      printf("Invalid Token, SET name expected\n");
      goto lblCleanup;
   }
   hSetFirst = g_myset_Set[token.value];

   /* command print */
   if (command == COMMAND_PRINT)
   {
      token = myset_GetNextToken(line, lineLength, &currentPosition);
      if (token.eType == END_OF_LINE)
      {
         myset_PrintSet(hSetFirst);
         goto lblCleanup;
      }
      printf("Invalid Token, end of line is expected\n");
      goto lblCleanup;
   }

   /* if its not stop or print, there should be a comma after the first set name */
   token = myset_GetNextToken(line, lineLength, &currentPosition);
   if (token.eType != COMMA)
   {
      printf("Invalid Token, comma expected after SET name\n");
      goto lblCleanup;
   }

   /* command read */
   if (command == COMMAND_READ)
   {
      int i = 0;
      int numbersArrSize = 0;
      numbersArr = NULL;

      token = myset_GetNextToken(line, lineLength, &currentPosition);
      while (token.eType != MINUS1)
      {
         if (token.eType == INVALID && token.value == -1)
         {
            printf("Invalid Token, cannot accept negative numbers other than -1 at the end\n");
            goto lblCleanup;
         }
         if (token.eType != NUMBER)
         {
            printf("Invalid Token, integer expected after comma\n");
            goto lblCleanup;
         }

         if (token.value > 127)
         {
            printf("Invalid Token, integer is larger than 127\n");
            goto lblCleanup;
         }

         /* dynamic memory allocation for the numbers input */
         if (i >= numbersArrSize)
         {
            int newSize = numbersArrSize + 1;
            int *newNumbersArr = NULL;
            newNumbersArr = realloc(numbersArr, sizeof(int) * newSize);
            if (NULL == newNumbersArr)
            {
               printf("Memory allocation failed\n");
               nRetVal = -1;
               goto lblCleanup;
            }
            numbersArr = newNumbersArr;
            numbersArrSize = newSize;
         }

         numbersArr[i] = token.value;
         i++;

         /* always a comma after an integer */
         token = myset_GetNextToken(line, lineLength, &currentPosition);
         if (token.eType != COMMA)
         {
            printf("Invalid Token, comma expected after number\n");
            goto lblCleanup;
         }
         token = myset_GetNextToken(line, lineLength, &currentPosition);
      }

      /* validating nothing after the '-1' at the end*/
      token = myset_GetNextToken(line, lineLength, &currentPosition);
      if (token.eType != END_OF_LINE)
      {
         printf("Invalid Token, end of line is expected\n");
         goto lblCleanup;
      }

      /* call SET_Assign with the integers array and then free the allocated memory*/
      SET_Assign(hSetFirst, numbersArr, i);
      goto lblCleanup;
   }

   /*from now on it must be an operation with 3 sets as params*/
   token = myset_GetNextToken(line, lineLength, &currentPosition);
   if (token.eType != TOKEN_SET)
   {
      printf("Invalid Token, second SET name expected\n");
      goto lblCleanup;
   }
   hSetSecond = g_myset_Set[token.value];

   token = myset_GetNextToken(line, lineLength, &currentPosition);
   if (token.eType != COMMA)
   {
      printf("Invalid Token, comma expected after second SET\n");
      goto lblCleanup;
   }

   token = myset_GetNextToken(line, lineLength, &currentPosition);
   if (token.eType != TOKEN_SET)
   {
      printf("Invalid Token, third SET name expected\n");
      goto lblCleanup;
   }
   hSetThird = g_myset_Set[token.value];

   token = myset_GetNextToken(line, lineLength, &currentPosition);
   if (token.eType != END_OF_LINE)
   {
      printf("Invalid Token, end of line is expected\n");
      goto lblCleanup;
   }

   /* we have the 3 sets, now we do the operation on the sets based on the command given */
   if (command == COMMAND_UNION)
   {
      SET_Union(hSetFirst, hSetSecond, hSetThird);
      goto lblCleanup;
   }

   if (command == COMMAND_INTERSECT)
   {
      SET_Intersect(hSetFirst, hSetSecond, hSetThird);
      goto lblCleanup;
   }

   if (command == COMMAND_SUB)
   {
      SET_Sub(hSetFirst, hSetSecond, hSetThird);
      goto lblCleanup;
   }

   if (command == COMMAND_SYMDIFF)
   {
      SET_Symdiff(hSetFirst, hSetSecond, hSetThird);
      goto lblCleanup;
   }

lblCleanup:
   if (NULL != line)
   {
      free(line);
   }
   if (NULL != numbersArr)
   {
      free(numbersArr);
   }
   return nRetVal;
}

/******************************************************************************
 * Func:    myset_GetNextToken
 * Purpose: Function to get the next token from the input line.
 * Params:  line - The input line from which to get the token.
 *          lineLength - The length of the input line.
 *          currentPosition - A pointer to the current position in the line.
 * Returns: The next token from the input line.
 * Remark: The function modifies the currentPosition to point to the next token.
 * Remark: The function returns an invalid token if the token is not recognized.
 ******************************************************************************/
static MYSET_TOKEN myset_GetNextToken(char *line, int lineLength, int *currentPosition)
{
   MYSET_TOKEN token;

   while (*currentPosition < lineLength)
   {
      int currentTokenPosition = *currentPosition;
      char currentChar = line[*currentPosition];
      int tokenLength;

      /* stop the loop, end of line reached, return end of line after the loop */
      if (currentChar == '\n')
      {
         break;
      }
      /* ignore whitespaces */
      if (currentChar == ' ' || currentChar == '\t')
      {
         (*currentPosition)++;
         continue;
      }
      if (line[*currentPosition] == ',')
      {
         /* Found a token - comma*/
         (*currentPosition)++;
         token.eType = COMMA;
         return token;
      }
      if (line[*currentPosition] == '-')
      {
         (*currentPosition)++;
         if (*currentPosition < lineLength && line[*currentPosition] == '1')
         {
            /* Found -1 token */
            (*currentPosition)++;
            token.eType = MINUS1;
            return token;
         }
         token.eType = INVALID;
         token.value = -1;
         return token;
      }

      /* find the end of the token */
      (*currentPosition)++;
      while (*currentPosition < lineLength && line[*currentPosition] != ' ' && line[*currentPosition] != '\t' && line[*currentPosition] != ',' && line[*currentPosition] != '\n')
      {
         (*currentPosition)++;
      }

      /* other token in between currentTokenPosition ... currentPosition-1*/
      tokenLength = (*currentPosition) - currentTokenPosition;
      if (tokenLength == 4 && strncmp(line + currentTokenPosition, "SETA", 4) == 0)
      {
         token.eType = TOKEN_SET;
         token.value = 0;
         return token;
      }
      else if (tokenLength == 4 && strncmp(line + currentTokenPosition, "SETB", 4) == 0)
      {
         token.eType = TOKEN_SET;
         token.value = 1;
         return token;
      }
      else if (tokenLength == 4 && strncmp(line + currentTokenPosition, "SETC", 4) == 0)
      {
         token.eType = TOKEN_SET;
         token.value = 2;
         return token;
      }
      else if (tokenLength == 4 && strncmp(line + currentTokenPosition, "SETD", 4) == 0)
      {
         token.eType = TOKEN_SET;
         token.value = 3;
         return token;
      }
      else if (tokenLength == 4 && strncmp(line + currentTokenPosition, "SETE", 4) == 0)
      {
         token.eType = TOKEN_SET;
         token.value = 4;
         return token;
      }
      else if (tokenLength == 4 && strncmp(line + currentTokenPosition, "SETF", 4) == 0)
      {
         token.eType = TOKEN_SET;
         token.value = 5;
         return token;
      }
      else if (tokenLength == 4 && strncmp(line + currentTokenPosition, "stop", 4) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_STOP;
         return token;
      }
      else if (tokenLength == 8 && strncmp(line + currentTokenPosition, "read_set", 8) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_READ;
         return token;
      }
      else if (tokenLength == 9 && strncmp(line + currentTokenPosition, "print_set", 9) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_PRINT;
         return token;
      }
      else if (tokenLength == 9 && strncmp(line + currentTokenPosition, "union_set", 9) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_UNION;
         return token;
      }
      else if (tokenLength == 13 && strncmp(line + currentTokenPosition, "intersect_set", 13) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_INTERSECT;
         return token;
      }
      else if (tokenLength == 7 && strncmp(line + currentTokenPosition, "sub_set", 7) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_SUB;
         return token;
      }
      else if (tokenLength == 11 && strncmp(line + currentTokenPosition, "symdiff_set", 11) == 0)
      {
         token.eType = COMMAND;
         token.value = COMMAND_SYMDIFF;
         return token;
      }
      else if (isdigit(line[currentTokenPosition]))
      {
         int number = 0;
         int i = 0;
         for (i = currentTokenPosition; i < *currentPosition; i++)
         {
            if (isdigit(line[i]))
            {
               number = number * 10 + (line[i] - '0');
            }
            else
            {
               token.eType = INVALID;
               return token;
            }
         }
         token.eType = NUMBER;
         token.value = number;
         return token;
      }
      else
      {
         token.eType = INVALID;
         return token;
      }
   }
   token.eType = END_OF_LINE;
   return token;
}
