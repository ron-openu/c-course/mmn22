/******************************************************************************
 * File:    set.c
 * Author:  Ron Hasson
 * Purpose: This file implements a set data structure that stores numbers in
 *          the range of 0 to 127. It provides functions for creating,
 *          manipulating, and performing operations on sets, such as union,
 *          intersection, difference, and symmetric difference.
 ******************************************************************************/

/******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include "set.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************
 *** TYPEDEFS
 ******************************************************************************/

/* Function pointer type for set operation functions. */
typedef unsigned int (*SET_OPERATOR_FNC)(unsigned int a, unsigned int b);

/******************************************************************************
 *** MACROS
 ******************************************************************************/

/* number of ints in order for the set to contain 128 bits (32bits * 4) */
#define SET_LENGTH_IN_INTS 4

/* number of bits in int variable */
#define SET_BITS_IN_INT 32

/******************************************************************************
 *** INTERNAL FUNCTIONS DECLARATIONS
 ******************************************************************************/
static void set_Zero(HSET hSet);
static int set_Length(HSET hSet);

/******************************************************************************
 *** STRUCTURES
 ******************************************************************************/

/*A structure representing a set of numbers between 0 to 127.*/
struct SET
{
    unsigned int nBit[SET_LENGTH_IN_INTS];
};

/******************************************************************************
 *** INTERNAL FUNCTIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    set_Zero
 * Purpose: This function takes a pointer to a set (HSET) and initializes all
 *          the bits in the set to zero.
 *          It uses the memset function to set all bytes in the set to zero,
 *          effectively clearing the set.
 * Params:  hSet - A pointer to the set to be initialized.
 * Returns: None.
 ******************************************************************************/
static void set_Zero(HSET hSet)
{
    memset(hSet, 0, sizeof(*hSet));
}

/******************************************************************************
 * Func:    set_Length
 * Purpose: This function calculates the length of a set.
 * Params:  hSet - A pointer to the set for which the length needs to be calculated.
 * Returns: The length of the set.
 ******************************************************************************/
static int set_Length(HSET hSet)
{
    int nLength = 0, i = 0, j = 0;
    for (i = 0; i < SET_LENGTH_IN_INTS; i++)
    {
        for (j = 0; j < SET_BITS_IN_INT; j++)
        {
            int mask = 1 << j;
            nLength += ((hSet->nBit[i] & mask) > 0) ? 1 : 0;
        }
    }
    return nLength;
}

/******************************************************************************
 * Func:    set_Operator
 * Purpose: Performs a set operation on two sets and stores the result in a third set.
 * Params:  hSetA - A handle to the first set.
 *          hSetB - A handle to the second set.
 *          hSetRes - A handle to the set where the result will be stored.
 *          pfnOperator - A function pointer to the set operator function.
 * Returns: None.
 ******************************************************************************/
static void set_Operator(HSET hSetA, HSET hSetB, HSET hSetRes, SET_OPERATOR_FNC pfnOperator)
{
    int i = 0;
    if (NULL == hSetA || NULL == hSetB || NULL == hSetRes)
    {
        return;
    }

    for (i = 0; i < SET_LENGTH_IN_INTS; i++)
    {
        hSetRes->nBit[i] = pfnOperator(hSetA->nBit[i], hSetB->nBit[i]);
    }
}

/******************************************************************************
 * Func:    set_UnionOperator
 * Purpose: Performs a bitwise OR operation on two unsigned integers.
 *          This function is used as an operator for set union operation.
 * Params:  a - An unsigned integer.
 *          b - An unsigned integer.
 * Returns: The result of the operation on 'a' and 'b'.
 ******************************************************************************/
static unsigned int set_UnionOperator(unsigned int a, unsigned int b)
{
    return a | b;
}

/******************************************************************************
 * Func:    set_IntersectOperator
 * Purpose: Performs a bitwise AND operation on two unsigned integers.
 *          This function is used as an operator for set intersection operation.
 * Params:  a - An unsigned integer.
 *          b - An unsigned integer.
 * Returns: The result of the operation on 'a' and 'b'.
 ******************************************************************************/
static unsigned int set_IntersectOperator(unsigned int a, unsigned int b)
{
    return a & b;
}

/******************************************************************************
 * Func:    set_SubOperator
 * Purpose: Performs a bitwise subtraction operation on two unsigned integers.
 *          Subtraction of 2 sets is all the elements of set a that dont exist in set b.
 *          This function is used as an operator for set subtraction operation.
 * Params:  a - An unsigned integer.
 *          b - An unsigned integer.
 * Returns: The result of the operation on 'a' and 'b'.
 ******************************************************************************/
static unsigned int set_SubOperator(unsigned int a, unsigned int b)
{
    return a & ~b;
}

/******************************************************************************
 * Func:    set_SymdiffOperator
 * Purpose: Performs a bitwise symmetric difference operation on two unsigned integers.
 *          The symmetric difference of two sets is the set of elements which are in
 *          either of the sets, but not in their intersection.
 *          This function is used as an operator for set subtraction operation.
 * Params:  a - An unsigned integer.
 *          b - An unsigned integer.
 * Returns: The result of the operation on 'a' and 'b'.
 ******************************************************************************/
static unsigned int set_SymdiffOperator(unsigned int a, unsigned int b)
{
    return (a | b) & ~(a & b);
}
/******************************************************************************
 *** EXTERNAL FUNCTIONS
 ******************************************************************************/

/* creates a new empty set */
HSET SET_Create()
{
    HSET hCreatedSet = NULL;
    hCreatedSet = malloc(sizeof(*hCreatedSet));
    if (NULL == hCreatedSet)
    {
        return NULL;
    }
    set_Zero(hCreatedSet);
    return hCreatedSet;
}

/* frees the given set from memory */
void SET_Free(HSET hSet)
{
    if (NULL == hSet)
    {
        return;
    }
    free(hSet);
}

/* inserts the numbers from the array into the set */
void SET_Assign(HSET hSet, int *anArray, int nArrayLength)
{
    int i = 0;
    int mask = 1;
    if (NULL == hSet)
    {
        return;
    }

    set_Zero(hSet);
    /* if the array is empty, just empty the set first and then return*/
    if (NULL == anArray)
    {
        return;
    }

    for (i = 0; i < nArrayLength; i++)
    {
        int nNumber = anArray[i];
        int nCell = nNumber / SET_BITS_IN_INT;
        int nInCell = nNumber % SET_BITS_IN_INT;

        mask = 1 << nInCell;

        hSet->nBit[nCell] |= mask;
    }
}

/* exports a set into an array */
int *SET_Export(HSET hSet, int *pnArrayLength)
{
    int i, j;
    int *set_array;
    int nLength = 0, arrayIndex = 0;

    nLength = set_Length(hSet);
    *pnArrayLength = nLength;

    set_array = malloc(nLength * sizeof(int));

    if (NULL == set_array)
    {
        return NULL;
    }

    for (i = 0; i < SET_LENGTH_IN_INTS; i++)
    {
        for (j = 0; j < SET_BITS_IN_INT; j++)
        {
            int bit = 0;
            int mask = 1 << j;
            bit = ((hSet->nBit[i] & mask) > 0) ? 1 : 0;
            if (bit)
            {
                set_array[arrayIndex] = i * SET_BITS_IN_INT + j;
                arrayIndex++;
            }
        }
    }

    return set_array;
}

/* performs a union operation between hSetA and hSetB and sets the result in hSetRes*/
void SET_Union(HSET hSetA, HSET hSetB, HSET hSetRes)
{
    set_Operator(hSetA, hSetB, hSetRes, set_UnionOperator);
}

/*performs an intersect operation between hSetA and hSetB and sets the result in hSetRes*/
void SET_Intersect(HSET hSetA, HSET hSetB, HSET hSetRes)
{
    set_Operator(hSetA, hSetB, hSetRes, set_IntersectOperator);
}

/*performs a subtract operation between hSetA and hSetB and sets the result in hSetRes*/
void SET_Sub(HSET hSetA, HSET hSetB, HSET hSetRes)
{
    set_Operator(hSetA, hSetB, hSetRes, set_SubOperator);
}

/*performs a symmetric difference operation between hSetA and hSetB and sets the result in hSetRes*/
void SET_Symdiff(HSET hSetA, HSET hSetB, HSET hSetRes)
{
    set_Operator(hSetA, hSetB, hSetRes, set_SymdiffOperator);
}