/******************************************************************************
 * File:    set.h
 * Author:  Ron Hasson
 * Purpose: This header file defines the interface for a set data structure
 *          that stores numbers in the range of 0 to 127. It provides functions
 *          for creating, manipulating, and performing operations on sets,
 *          such as union, intersection, difference, and symmetric difference.
 ******************************************************************************/
#ifndef SET_H
#define SET_H

/******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdlib.h>

/******************************************************************************
 *** TYPEDEFS
 ******************************************************************************/

/*A structure representing a set of numbers between 0 to 127.*/
typedef struct SET SET;

/*A pointer to a SET structure.*/
typedef SET *HSET;

/******************************************************************************
 *** EXTERNAL FUNCTIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    SET_Create
 * Purpose: creates a new empty set
 * Params:  None
 * Returns: handle to the newly-created set. NULL in case of error.
 * Remark:  In case of success, the user MUST call SET_Free to free the set.
 ******************************************************************************/
HSET SET_Create();

/******************************************************************************
 * Func:    SET_Free
 * Purpose: frees the given set from memory
 * Params:  hset - pointer to a set to free
 * Returns: None
 ******************************************************************************/
void SET_Free(HSET hset);

/******************************************************************************
 * Func:    SET_Assign
 * Purpose: inserts the numbers from the array into the set
 * Params:  hset - pointer to a set to insert to
 *          anArray - an array of numbers from 0 to 127 to insert to the set
 *          nArrayLength - the length of the aforementioned array
 * Returns: None
 ******************************************************************************/
void SET_Assign(HSET hSet, int *anArray, int nArrayLength);

/******************************************************************************
 * Func:    SET_Export
 * Purpose: exports a set into an array
 * Params:  hset - pointer to a set to export
 *          pnArrayLength - a pointer to an integer that will get assigned the array's length
 * Returns: a pointer to a numbers array that represents the set
 ******************************************************************************/
int *SET_Export(HSET hSet, int *pnArrayLength);

/******************************************************************************
 * Func:    SET_Union
 * Purpose: performs a union operation between hSetA and hSetB
 *          and sets the result in hSetRes
 * Params:  hSetA - pointer to a set to perform the operation on (wont change)
 *          hSetB - pointer to a set to perform the operation on (wont change)
 *          hSetRes - pointer to a set that will contain the result of the operation
 * Returns: None
 ******************************************************************************/
void SET_Union(HSET hSetA, HSET hSetB, HSET hSetRes);

/******************************************************************************
 * Func:    SET_Intersect
 * Purpose: performs an intersect operation between hSetA and hSetB
 *          and sets the result in hSetRes
 * Params:  hSetA - pointer to a set to perform the operation on (wont change)
 *          hSetB - pointer to a set to perform the operation on (wont change)
 *          hSetRes - pointer to a set that will contain the result of the operation
 * Returns: None
 ******************************************************************************/
void SET_Intersect(HSET hSetA, HSET hSetB, HSET hSetRes);

/******************************************************************************
 * Func:    SET_Sub
 * Purpose: performs a subtract operation between hSetA and hSetB
 *          and sets the result in hSetRes
 * Params:  hSetA - pointer to a set to perform the operation on (wont change)
 *          hSetB - pointer to a set to perform the operation on (wont change)
 *          hSetRes - pointer to a set that will contain the result of the operation
 * Returns: None
 ******************************************************************************/
void SET_Sub(HSET hSetA, HSET hSetB, HSET hSetRes);

/******************************************************************************
 * Func:    SET_Symdiff
 * Purpose: performs a symmetric difference operation between hSetA and hSetB
 *          and sets the result in hSetRes
 * Params:  hSetA - pointer to a set to perform the operation on (wont change)
 *          hSetB - pointer to a set to perform the operation on (wont change)
 *          hSetRes - pointer to a set that will contain the result of the operation
 * Returns: None
 ******************************************************************************/
void SET_Symdiff(HSET hSetA, HSET hSetB, HSET hSetRes);

#endif